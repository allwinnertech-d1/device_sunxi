# OpenHarmony 构建 d1_nezha

## 概述

【哪吒】是全志在线基于全志科技D1芯片定制的AIoT开发板，是全球首款支持64bit RISC-V指令集并支持Linux系统的可量产开发板。

本文介绍如何在OpenHarmony平台上构建d1固件。

版本说明： 

- 基于OH 3.0 LTS，支持D1内核与OH rootfs的构建。

后续开发计划：

- 添加wayland支持。
- 添加基础服务支持。
- ...

## 源码获取

安装下面方法获取源码。由于OpenHarmony更新较频繁，为保证稳定，在sunxi_d1.xml中对各仓库指定commit下载。

```
repo init -u https://gitee.com/allwinnertech-d1/manifest-sunxi-d1.git -b master -m sunxi_d1.xml --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

源码仓库主要有三个来源：

1.  OpenHarmony官方仓库 - https://gitee.com/openharmony
2.  iscas-taiyang支持riscv的仓库 - https://gitee.com/iscas-taiyang
3.  D1平台相关仓库 - https://gitee.com/allwinnertech-d1

## 构建方法

当前仅支持在OpenHarmony上构建D1内核与rootfs，需要借助D1的Tina环境中的工具链。

### Tina Linux D1环境下载

参考 https://d1.docs.aw-ol.com/ 下载D1的Tina Linux SDK

### OpenHarmony D1 构建

假定tina-d1为D1 Tina Linux环境根目录；假定OpenHarmony为D1 OpenHarmony根目录。

- cd OpenHarmony，运行./build/prebuilts_download.sh，下载相关编译工具链与host端工具
- 将tina-d1/prebuilt/gcc/linux-x86/riscv/toolchain-thead-glibc/riscv64-glibc-gcc-thead_20200702复制到OpenHarmony/prebuilts/gcc/linux-x86/riscv/riscv64-glibc-gcc-thead_20200702
- 从 https://pan.baidu.com/s/19JVNwFrl5ISOAsruW_y9hA 提取码: chds 下载llvm-riscv工具链llvm-riscv-1124.tar.gz（临时使用），解压到OpenHarmony/prebuilts/clang/ohos/linux-x86_64/llvm-riscv/
- ./build.sh --product-name sunxi_d1 --ccache

至此，OpenHarmony环境下的D1内核与rootfs构建完成，成功的log如下所示。

```
c targets overlap rate statistics
subsystem               files NO.       percentage      builds NO.      percentage      overlap rate
third_party                 6917        55.7%       7078        56.2%   1.02
aafwk                        186        1.5%         186        1.5%    1.00
account                       14        0.1%          14        0.1%    1.00
ace                          766        6.2%         766        6.1%    1.00
appexecfwk                   365        2.9%         365        2.9%    1.00
ccruntime                     17        0.1%          17        0.1%    1.00
communication                462        3.7%         462        3.7%    1.00
developtools                  77        0.6%          77        0.6%    1.00
distributeddatamgr           314        2.5%         314        2.5%    1.00
distributedhardware           33        0.3%          33        0.3%    1.00
distributedschedule           40        0.3%          40        0.3%    1.00
global                        19        0.2%          19        0.2%    1.00
graphic                      133        1.1%         133        1.1%    1.00
hdf                           44        0.4%          44        0.3%    1.00
hiviewdfx                    135        1.1%         135        1.1%    1.00
miscservices                  62        0.5%          62        0.5%    1.00
multimedia                   279        2.2%         279        2.2%    1.00
multimodalinput               29        0.2%          29        0.2%    1.00
notification                 127        1.0%         127        1.0%    1.00
powermgr                      50        0.4%          50        0.4%    1.00
security                     268        2.2%         268        2.1%    1.00
startup                       57        0.5%          57        0.5%    1.00
telephony                    225        1.8%         225        1.8%    1.00
utils                        211        1.7%         211        1.7%    1.00
wpa_supplicant-2.9           149        1.2%         149        1.2%    1.00

c overall build overlap rate: 1.01


post_process
=====build sunxi_d1 successful.
2021-12-01 15:21:57
++++++++++++++++++++++++++++++++++++++++

```

### 打包sunxi_d1固件

在SDK根目录执行: ./device/sunxi/build/pack，生成out/d1-nezha/tina_d1-nezha_uart0.img文件，将此文件烧录到D1开发板即可。

- 烧写参考链接[D1编译和烧写](https://d1.docs.aw-ol.com/study/study_4compile/)
- 默认打包的rootfs是squashfs类型，只读，不可写。执行: ./device/sunxi/build/pack -e，可生成ext4格式(可写)的rootfs，此时固件大小会超过256M（D1开发板flash总大小），无法直接烧写，需使用TF卡烧写方式。
- [TF卡烧写工具:PhoenixCardv4.2.7.7z](https://www.aw-ol.com/downloads?cat=5)登录后下载，烧写时将TF卡设定为启动卡。

如果打包过程中出现下面这类log，文件超过了分区的大小，修改device/sunxi/config/chips/d1/configs/nezha/sys_partition.fex，调大对应分区的size即可。
```
ERROR: dl file rootfs.fex size too large
ERROR: filename = rootfs.fex
ERROR: dl_file_size = 254720 sector
ERROR: part_size = 40824 sector
ERROR: update mbr file fail
ERROR: update_mbr failed
```

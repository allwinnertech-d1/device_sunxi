# (c) Copyright 2021 Allwinner
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
LICHEE_CHIP:=sun20iw1p1
LICHEE_PRODUCT:=
LICHEE_BOARD:=
LICHEE_FLASH:=
LICHEE_ARCH:=riscv
LICHEE_BRANDY_VER:=2.0
LICHEE_BRANDY_DEFCONF:=sun20iw1p1_defconfig
LICHEE_KERN_VER:=5.4
LICHEE_KERN_DEFCONF:=sun20iw1p1_d1_defconfig
LICHEE_BUILDING_SYSTEM:=buildroot
LICHEE_BR_VER:=201902
LICHEE_BR_DEFCONF:=sun20i_defconfig
LICHEE_COMPILER_TAR:=riscv64-glibc-gcc-thead_20200702.tar.xz
LICHEE_REDUNDANT_ENV_SIZE:=0x20000

# (c) Copyright 2021 Allwinner
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
LICHEE_CHIP:=sun8iw19p1
LICHEE_PRODUCT:=
LICHEE_BOARD:=
LICHEE_ARCH:=arm
LICHEE_FLASH:=nor
LICHEE_BRANDY_VER:=2.0
LICHEE_BRANDY_DEFCONF:=sun8iw19p1_defconfig
LICHEE_KERN_VER:=4.9
LICHEE_KERN_DEFCONF:=config-4.9
LICHEE_BUILDING_SYSTEM:=
LICHEE_BR_VER:=
LICHEE_BR_DEFCONF:=sun8i_defconfig
LICHEE_COMPILER_TAR:=gcc-linaro-5.3.1-2016.05-x86_64_arm-linux-gnueabi.tar.xz
LICHEE_ROOTFS:=target-arm-linaro-5.3.tar.bz2

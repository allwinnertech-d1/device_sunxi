# (c) Copyright 2021 Allwinner
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# define the verions of the image
# format: main.sub
# such as 1.01, 2.33
# NOTICE: the range of main version is from 0 to 31,
#         the range of sub  version is from 0 to 63
# when you change the version, you must increase one of them or both, and never reduce the versions.
# the default version is 0.0

MAIN_VERSION = 0


